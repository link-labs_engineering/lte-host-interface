#ifndef __LTE_OPCODES_H__
#define __LTE_OPCODES_H__

#include <stdint.h>

#define LTE_IFC_VERSION_MAJOR (0)
#define LTE_IFC_VERSION_MINOR (0)
#define LTE_IFC_VERSION_TAG (1)

typedef enum
{
    OP_VERSION = 0,                   ///< 0x00
    OP_IFC_VERSION = 1,               ///< 0x01
    OP_IMEI_GET = 2,                  ///< 0x02
    OP_IRQ_FLAGS = 15,                ///< 0x0F
//    OP_IRQ_FLAGS_MASK = 16,           ///< 0x10
    OP_MSG_RECV = 43,                 ///< 0x2B
    OP_SETTINGS_SAVE = 51,            ///< 0x33
    OP_SETTINGS_DELETE = 52,          ///< 0x34
    OP_RESET_MCU = 60,                ///< 0x3C
    OP_STATE_GET = 61,                ///< 0x3D
    OP_UPLINK_SEND = 92,              ///< 0x5C
    OP_UPLINK_STATUS = 93,            ///< 0x5D
    OP_RX_MODE_SET = 110,             ///< 0x6E
    OP_RX_MODE_GET = 111,             ///< 0x6F
    OP_FOTA_PERIOD_SET = 112,         ///< 0x70
    OP_FOTA_PERIOD_GET = 113,         ///< 0x71
    OP_FOTA_CHECK_AT_BOOT_SET = 114,  ///< 0x72
    OP_FOTA_CHECK_AT_BOOT_GET = 115,  ///< 0x73
    OP_PWR_POLICY_SET = 116,          ///< 0x74
    OP_PWR_POLICY_GET = 117,          ///< 0x75
    OP_NETWORK_INFO_GET = 118,        ///< 0x76
    OP_NETWORK_INFO_UPDATE = 119,     ///< 0x77
    OP_MAILBOX_REQUEST = 129,         ///< 0x81
    OP_TEST_FOTA_DL = 200,            ///< 0xC8
    OP_TEST_FOTA_GET_STATE = 201,     ///< 0xC9
    OP_DEBUG_MSG = 202,               ///< 0xCA
    OP_ASSERT_GET = 248,              ///< 0xF8
    OP_ASSERT_SET = 249,              ///< 0xF9
    OP_TRIGGER_BOOTLOADER = 250,      ///< 0xFA
    OP_HARDWARE_TYPE = 254,           ///< 0xFE
    OP_FIRMWARE_TYPE = 255            ///< 0xFF
} opcode_t;



#ifndef VERSION_LEN
#define VERSION_LEN (4)
#endif

/**
 * Firmware identifiers
 */

typedef struct {
    uint16_t cpu_code;
    uint16_t functionality_code;
} ll_firmware_type_t;
#define FIRMWARE_TYPE_LEN           (4)

/** Possible downlink modes for OP_DOWNLINK_MODE */
typedef enum
{
    DOWNLINK_MODE_OFF = 0,          ///< 0x00
    DOWNLINK_MODE_ALWAYS_ON = 1,    ///< 0x01
    DOWNLINK_MODE_MAILBOX = 2,      ///< 0x02
    NUM_DOWNLINK_MODES
//    DOWNLINK_MODE_PERIODIC = 3,     ///< 0x03
//    DOWNLINK_MODE_FAILURE = 255,    ///< 0xFF
} downlink_mode_t;

#endif /* __LTE_OPCODES_H__ */
