#ifndef __LTE_BYTEDEFS_H__
#define __LTE_BYTEDEFS_H__

// Byte position for Master Commands
#define HOST_IFC_CMD_IDX_SOF                (0)
#define HOST_IFC_CMD_IDX_CMD                (1)
#define HOST_IFC_CMD_IDX_VER                (2)
#define HOST_IFC_CMD_IDX_MSG_NUM            (3)
#define HOST_IFC_CMD_IDX_PAYLOAD_LEN_MSB    (4)
#define HOST_IFC_CMD_IDX_PAYLOAD_LEN_LSB    (5)
#define HOST_IFC_CMD_IDX_PAYLOAD            (6)

// Byte position for Slave Responses

#define HOST_IFC_RSP_IDX_SOF                (0)
#define HOST_IFC_RSP_IDX_CMD                (1)
#define HOST_IFC_RSP_IDX_MSG_NUM            (2)
#define HOST_IFC_RSP_IDX_ACK                (3)
#define HOST_IFC_RSP_IDX_PAYLOAD_LEN_MSB    (4)
#define HOST_IFC_RSP_IDX_PAYLOAD_LEN_LSB    (5)
#define HOST_IFC_RSP_IDX_PAYLOAD            (6)

// Header lengths
#define HOST_IFC_CMD_HEADER_LEN             (6)
#define HOST_IFC_RESP_HEADER_LEN            (6)

// Special codes
#define HOST_IFC_BYTE_SOF                   (0xC5)

#define HOST_IFC_CRC_LEN                    (2)

// Part of OP_MSG_SEND
#define MAX_USER_UPLINK_LENGTH_BYTES        (1280)               //
#define MAX_USER_DOWNLINK_LENGTH_BYTES      (257)               // 1 byte port + 256 byte payload

#define HOST_IFC_MAX_CMD_PAYLOAD_LEN        (MAX_USER_UPLINK_LENGTH_BYTES)
#define HOST_IFC_MAX_RSP_PAYLOAD_LEN        (MAX_USER_DOWNLINK_LENGTH_BYTES)

// FIFO sizes
#define NUM_BYTES_LONGEST_CMD          (HOST_IFC_MAX_CMD_PAYLOAD_LEN + HOST_IFC_CMD_IDX_PAYLOAD + 1 + HOST_IFC_CRC_LEN)
#define NUM_BYTES_LONGEST_RSP          (HOST_IFC_MAX_RSP_PAYLOAD_LEN + HOST_IFC_RSP_IDX_PAYLOAD + HOST_IFC_CRC_LEN)

#define NUM_BYTES_RX_FIFO              (NUM_BYTES_LONGEST_CMD + 4 + 1)  // We send 4 0xFF's at the beginning of every command, and we need another byte
                                                                        // for when the buffer is exactly full (to disambiguate the put and get indices).

#define HOST_IFC_RESPONSE_DEADLINE_MS           (300)   // Slave must respond to a command within this many ms
#define HOST_IFC_RESPONSE_DEADLINE_TICKS    (HOST_IFC_RESPONSE_DEADLINE_MS/portTICK_RATE_MS)


/* When modifying or adding to any of these NACKs,
 * update lte_ifc_ack_codes in lte_ifc.h manually,
 * also add a string to lte_ifc_ack_desc() */

#define LTE_IFC_NACK_CMD_NOT_SUPPORTED       (1)   ///< Command not supported.
#define LTE_IFC_NACK_INCORRECT_CHKSUM        (2)   ///< Incorrect Checksum.
#define LTE_IFC_NACK_PAYLOAD_LEN_OOR         (3)   ///< Length of payload sent in command was out of range.
#define LTE_IFC_NACK_PAYLOAD_OOR             (4)   ///< Payload sent in command was out of range.
#define LTE_IFC_NACK_BOOTUP_IN_PROGRESS      (5)   ///< Not allowed since firmware bootup still in progress. Wait.
#define LTE_IFC_NACK_BUSY_TRY_AGAIN          (6)   ///< Operation prevented by temporary event. Re-try should work.
#define LTE_IFC_NACK_PAYLOAD_LEN_EXCEEDED    (7)   ///< Payload length is greater than the max supported length
#define LTE_IFC_NACK_NOT_IN_MAILBOX_MODE     (8)   ///< Module must be in DOWNLINK_MAILBOX mode
#define LTE_IFC_NACK_PAYLOAD_BAD_PROPERTY    (9)   ///< Invalid property specified in command
#define LTE_IFC_NACK_NODATA                  (10)  ///< No data is available to be returned
#define LTE_IFC_NACK_CMD_VER_NOT_SUPPORTED   (11)  ///< Command version is not supported
#define LTE_IFC_NACK_OTHER                   (99)

#endif // __LTE_BYTEDEFS_H__
