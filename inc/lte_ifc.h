#ifndef __LTE_IFC_H
#define __LTE_IFC_H

#include "lte_ifc_hal.h"
#include "lte_ifc_irq_flags.h"

#ifdef __cplusplus
extern "C" {
#endif

// The LTE-M UART connection is usually at 115200 baud
#define LTE_TTY_DEFAULT_BAUDRATE 115200

/**
 * @addtogroup lte_ifc_ack_code lte_ifc_ack_code
 *
 * @brief A common result code for most functions.
 *
 * An lte_ifc_ack_code is an int32_t type where:
 * - 0 is a success or error-free result.
 * - A negative value is a problem or error result.
 *
 * One can always get a descriptive text string for any error code using
 * the function @ref lte_ifc_ack_desc.
 *
 * @{
 */

#define LTE_IFC_ACK                                 (0)   ///< Success, or error-free condition.
#define LTE_IFC_ACK_CODE_CMD_NOT_SUPPORTED         (-1)   ///< Command not supported.
#define LTE_IFC_ACK_CODE_INCORRECT_CHKSUM          (-2)   ///< Incorrect Checksum.
#define LTE_IFC_ACK_CODE_PAYLOAD_LEN_OOR           (-3)   ///< Length of payload sent in command was out of range.
#define LTE_IFC_ACK_CODE_PAYLOAD_OOR               (-4)   ///< Payload sent in command was out of range.
#define LTE_IFC_ACK_CODE_BOOTUP_IN_PROGRESS        (-5)   ///< Not allowed since firmware bootup still in progress. Wait.
#define LTE_IFC_ACK_CODE_BUSY_TRY_AGAIN            (-6)   ///< Operation prevented by temporary event. Re-try should work.
#define LTE_IFC_ACK_CODE_PAYLOAD_LEN_EXCEEDED      (-7)   ///< Payload length is greater than the max supported length
#define LTE_IFC_ACK_CODE_NOT_IN_MAILBOX_MODE       (-8)   ///< Module must be in DOWNLINK_MAILBOX mode
#define LTE_IFC_ACK_CODE_PAYLOAD_BAD_PROPERTY      (-9)   ///< Invalid property specified in command
#define LTE_IFC_ACK_CODE_NODATA                    (-10)  ///< No data is available to be returned
#define LTE_IFC_ACK_CODE_CMD_VER_NOT_SUPPORTED     (-11)  ///< Command version is not supported
#define LTE_IFC_ACK_CODE_OTHER                     (-99)  ///< Unknown error.

#define LTE_IFC_ACK_CODE_LEN_OOR                   (-102) ///< Length is out of range.
#define LTE_IFC_ACK_CODE_QUEUE_FULL_BYTES          (-103) ///< Queue full because no more room in buffer
#define LTE_IFC_ACK_CODE_QUEUE_FULL_NUM            (-104) ///< Queue full due to max num packets
#define LTE_IFC_ACK_CODE_CALLER_PRIO_TOO_HIGH      (-105) ///< Caller task priority too high
#define LTE_IFC_ACK_CODE_PDP_TIMEOUT               (-106) ///< Timeout establishing Packet Data Protocol Context
#define LTE_IFC_ACK_CODE_REG_TIMEOUT               (-107) ///< Timeout registering with LTE network
#define LTE_IFC_ACK_CODE_CONNECT_TIMEOUT           (-109) ///< Timeout connecting to remote srver
#define LTE_IFC_ACK_CODE_TX_TIMEOUT                (-110) ///< Timeout trying to transmit data
#define LTE_IFC_ACK_CODE_HANGUP_IN_DIAL            (-113) ///< Unexpected socket hangup in dial state
#define LTE_IFC_ACK_CODE_ERR_MUTEX                 (-119) ///< Error with mutex ownership
#define LTE_IFC_ACK_CODE_HANGUP_IN_SOCK_OPEN       (-120) ///< Unexpected socket hangup in sock open state
#define LTE_IFC_ACK_CODE_SEND_WITHOUT_CONN         (-121) ///< Error attempting to send data without connection
#define LTE_IFC_ACK_CODE_SEND_DURING_LISTEN        (-122) ///< Error attempting to send data while in listen state
#define LTE_IFC_ACK_CODE_FOTA_IN_PROGRESS          (-123) ///< Action unavailable while FOTA is in progress
#define LTE_IFC_ACK_CODE_RX_TIMEOUT                (-124) ///< Timeout waiting for receive data
#define LTE_IFC_ACK_CODE_TIMEOUT_SERVER_RSP        (-125) ///< Timeout waiting for response from server
#define LTE_IFC_ACK_CODE_ERR_SERVER_RSP_ERR        (-126) ///< Received error waiting from response from server
#define LTE_IFC_ACK_CODE_ERR_SERVER_RSP_CODE       (-127) ///< Receiver error code from server
#define LTE_IFC_ACK_CODE_IFC_QUEUE_PUSH_FAIL       (-128) ///< Failed trying to push onto the interface queue
#define LTE_IFC_ACK_CODE_ERR_UNKNOWN               (-129) ///< Unknown error
#define LTE_IFC_ACK_CODE_ERR_NOT_REGISTERED        (-130) ///< Action unavailable while not registered
#define LTE_IFC_ACK_CODE_ERR_INIT_IN_PROGRESS      (-131) ///< Action unavailable while initialization in progress

#define LTE_IFC_ACK_CODE_INCORRECT_PARAMETER       (-201) ///< The parameter value was invalid.
#define LTE_IFC_ACK_CODE_INCORRECT_RESPONSE_LENGTH (-202) ///< Module response was not the expected size.
#define LTE_IFC_ACK_CODE_MESSAGE_NUMBER_MISMATCH   (-203) ///< Message number in response doesn't match expected
#define LTE_IFC_ACK_CODE_CHECKSUM_MISMATCH         (-204) ///< Checksum mismatch
#define LTE_IFC_ACK_CODE_COMMAND_MISMATCH          (-205) ///< Command mismatch (responding to a different command)
#define LTE_IFC_ACK_CODE_HOST_INTERFACE_TIMEOUT    (-206) ///< Timed out waiting for Rx bytes from interface
#define LTE_IFC_ACK_CODE_BUFFER_TOO_SMALL          (-207) ///< Response larger than provided output buffer
#define LTE_IFC_ACK_CODE_START_OF_FRAME            (-208) ///< transport_read failed getting FRAME_START
#define LTE_IFC_ACK_CODE_HEADER                    (-209) ///< transport_read failed getting header
#define LTE_IFC_ACK_CODE_TIMEOUT                   (-210) ///< The operation timed out.
#define LTE_IFC_ACK_CODE_INCORRECT_MESSAGE_SIZE    (-211) ///< The message size from the device was incorrect.
#define LTE_IFC_ACK_CODE_NO_NETWORK                (-212) ///< No network was available.
#define LTE_IFC_ACK_CODE_WAKEUP_TIMEOUT            (-213) ///< Wakeup Timeout.
#define LTE_IFC_ACK_CODE_WAKEUP_SET                (-214) ///< Setting wakeup request
#define LTE_IFC_ACK_CODE_WAKEUP_GET                (-215) ///< Getting wakeup status
#define LTE_IFC_ACK_CODE_IRQFLAGS_GPIO_GET         (-216) ///< Getting IRQ Flags GPIO line state
#define LTE_IFC_ACK_CODE_WRITE                     (-217) ///< Error was returned from hal_write
#define LTE_IFC_ACK_CODE_GETTIME                   (-218) ///< Error was returned from hal_gettime

/** @} (end addtogroup LTE_IFC_Ack_Code) */

#define LTE_IFC_SUCCESS LTE_IFC_ACK

/**
 * version struct
 */
typedef struct {
    uint8_t major;
    uint8_t minor;
    uint16_t tag;
} lte_version_t;


/**
* @addtogroup Link_Labs_LTE_Interface_Library lte_ifc
*
* @brief External host library which simplifies communication with an LTE Cat-M1
*      network using a Link Labs module.
* 
* This library runs on an external host processor which can be nearly any
* microcontroller or PC with a UART interface.  The library
* is written in C and contains two categories of functions:
* -# @ref lte_ifc_common These are portable functions,
* which are meant to run on any platform. These should be compiled as is with no modifications. These
* functions are the API which you should call from your application.
*
* -# @ref lte_ifc_hal A set of function prototypes for platform-specific and low-level operations.
* These are prototypes only - you must implement your own version of these
* and link them in. Usually these are extremely simple operations, like setting an I/O line high or low,
* or queueing some bytes for transmission through a UART, etc.
* 
* A block diagram of how the two categories fit into an integrated system is shown below.
* \htmlonly <style>div.image img[src="lte_ifc_block.png"]{width:400px;}</style> \endhtmlonly 
* @image html lte_ifc_block.png 
* 
* @{
*/

/** @addtogroup lte_ifc_common lte_ifc_common
 *  @{
 */

/**
 * @brief
 *   Force the module to reset and enter bootloader mode (takes a few seconds).
 *
 * @details
 *   This function forces the module to reset and enter bootloader mode. The module only resets
 *   and enters bootloader mode if the success code is returned.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_bootloader_mode(void);

/**
* @brief
*   Gets assert info from the module, if any is available.
*
* @return
*   @ref lte_ifc_ack_code
*/
int32_t lte_ifc_get_assert_info(char *filename, uint32_t* p_line, uint32_t* p_uptime_sec);

/**
 *@brief
 *  Reads out the IMEI of the device.
 *
 * @param[out] p_imei
 * The IMEI is returned in hex format. The IMEI is 15 decimal digits, so when represented in hex
 *   the length will be 13 hex digits. For example, an IMEI of 357353080026273 will be returned
 *   in uint64_t format as 0x00014502bf64f4a1.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_imei_get(uint64_t* p_imei);


/**
 *@brief
 *  Commands the module to check the downlink mailbox. This triggers an uplink transmission containing
 *  the mailbox request.
 *
 * @param[out] p_packet_id
 * If the success code is returned, this address will be populated with a packet id for the mailbox request.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_mailbox_request(uint32_t* p_packet_id);

/**
 * @brief
 *   Request the module to send an uplink message.
 *
 * @param[in] buf
 *   A byte array containing the data payload.
 *
 * @param[in] len
 *   Length of the input buffer in bytes. Must be less than or equal to
 *   MAX_USER_UPLINK_LENGTH_BYTES.
 *
 * @param[in] port
 *   Reserved for future use.
 *
 * @param[out] p_packet_id
 *   The address pointed to by this pointer will be populated with a packet id for the uplink message.
 *
 * @return
 *   @ref lte_ifc_ack_code Note that the success code only indicates if the uplink message was succesfully
 *   queued for transmission, not whether it was successfully transmitted. In order to check the delivery status
 *   the @ref lte_ifc_message_status function should be called.
 */
int32_t lte_ifc_message_send(uint8_t* buf, uint16_t len, uint8_t port, uint32_t* p_packet_id);

/**
 * @brief
 *   Request Check for a status update on pending uplink transmissions.
 *
 * @param[out] p_new_status
 *   This will be set to 1 if a new packet status is available, 0 otherwise.
 *
 * @param[out] p_packet_id
 *   The packet id that the status applies to. Only set if new_status is 1.
 *
 * @param[out] p_packet_status
 *  @ref lte_ifc_ack_code indicating the result of the transmission attempt on the packet. Only set if new_status is 1.
 *
 * @param[out] p_packet_time_in_queue_ms
 *   How long (in milliseconds) the packet was queued in the module, before it was able to be sent. Only set if new_status is 1.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_message_status(uint8_t* p_new_status, uint32_t* p_packet_id, int32_t* p_packet_status, uint32_t* p_packet_time_in_queue_ms);


/**
 * @brief
 *   Request Check for a status update on pending uplink transmissions. Returns a more detailed
 *   status than lte_ifc_message_status().
 *
 * @param[out] p_new_status
 *   This will be set to 1 if a new packet status is available, 0 otherwise.
 *
 * @param[out] p_packet_id
 *   The packet id that the status applies to. Only set if new_status is 1.
 *
 * @param[out] p_packet_status
 *  @ref lte_ifc_ack_code indicating the result of the transmission attempt on the packet. Only set if new_status is 1.
 *
 * @param[out] p_packet_time_in_queue_ms
 *   How long (in milliseconds) the packet was queued in the module, before it was able to be sent. Only set if new_status is 1.
 *
 * @param[out] p_i16_rsrq_db
 *   The Reference Signal Received Quality (RSRQ) in dB, corresponding to the cell that the message was sent through.
 *   Only set if new_status is 1.
 *
 * @param[out] p_i16_rsrp_dbm
 *   The Reference Signal Received Power (RSRP) in dBm, corresponding to the cell that the message was sent through.
 *   Only set if new_status is 1.
 *
 * @param[out] p_u32_cell_id
 *   The 4-byte E-UTRAN Cell Id of the cell that the message was sent through. Only set if new_status is 1.
 *
 * @param[out] p_u16_cell_tac
 *   The Tracking Area Code of the cell that the message was sent through. Only set if new_status is 1.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_message_status_ext(uint8_t* p_new_status, uint32_t* p_packet_id, int32_t* p_packet_status, uint32_t* p_packet_time_in_queue_ms,
                               int16_t* p_i16_rsrq_db, int16_t* p_i16_rsrp_dbm, uint32_t* p_u32_cell_id, uint16_t* p_u16_cell_tac);

/**
 * @brief
 *   Get the module firmware version number.
 *
 * @param[inout] version
 *   Pass in a pointer to a lte_version_t data type. This data structure will be populated if the ack code indicates success.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_version_get(lte_version_t *version);

 /**
 * @brief
 *   Save configurable settings to flash.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_settings_save(void);

/**
 * @brief
 *   Delete all settings from flash storage, restoring factory defaults on all configurable values.
 *
 * @return
 *   @ref lte_ifc_ack_code
 *
 */
int32_t lte_ifc_settings_delete(void);

/**
 * @brief
 *   Force the module to reset (if safe to do so).
 *
 * @details
 *   The module only resets if an ACK code is returned.
 *   If any other code is returned, the reset command was rejected. In this case, the function should be called
 *   again some time later (e.g. 1 second) until an ACK code is returned. The module should
 *   eventually return an ACK, although it may take as much as 30-45 seconds in the worst case.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_reset_mcu(void);

/**
 * @brief
 *   Force the module to reset (unconditionally).
 *
 * @details
 *   This function is not recommended to be used regularly or as a first resort to reset.
 *   See lte_ifc_reset_mcu() - which should be used first instead. This function is provided as a last resort for
 *   rare cases. The LTE-M SoC needs to be properly shutdown rather than having its power cut abruptly. Failure
 *   to do so may cause corruption in rare cases.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_reset_mcu_force(void);

/**
* @brief
*   Triggers an assert (for test purposes only).
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_trigger_assert(void);

/**
* @brief
*   Set received mode.
*
* @param[in] rx_mode
*   Reserved for future use.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_receive_mode_set(uint8_t rx_mode);

/**
* @brief
*   Get received mode.
*
* @param[out] p_rx_mode
*   Reserved for future use.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_receive_mode_get(uint8_t *p_rx_mode);

/**
 * @brief
 *   Retrieves a downlink message received by the module.
 *
 * @param[out] buf
 *   The buffer into which the received message will be placed. This buffer must
 *   be at least 1 byte larger than the maximum message size expected.
 *
 * @param[in,out] size
 *   The size of the output buffer. If the message is successfully retrieved,
 *   this will be set to the size of the message (not including the port).
 *
 * @param[out] port
 *   The port number associated with the message.
 *
* @return
*   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_retrieve_message(uint8_t *buf, uint16_t *size, uint8_t *port);

/**
 * @brief
 *   Retrieves a firmware-over-the-air image.
 *
 * @param[in] filename
 *   The ASCII filename of the file to download. Does not need to be NULL-terminated,
 *   since the length is also passed in. Can only contain either alphanumeric
 *   characters or a character in the set {-._}.
 *
 * @param[in] len
 *   Length of the filename (number of ASCII chars). This is needed since the 
 *   filename argument is not NULL-terminated.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_fota_download(uint8_t *filename, uint16_t len);

/**
 * @brief
 *   Retrieves the state of a firmware-over-the-air download.
 *
 * @param[out] p_state
 *   The state of the FOTA process.
 *
 * @param[out] p_end_reason
 *   The result of the last run FOTA process since power-on reset.
 *
 * @param[out] p_num_blocks_received
 *   The number of blocks received.
 *
 * @param[out] p_num_requests_sent
 *   The number of file requests sent.
 *
 * @param[out] p_transfer_complete
 *   Whether the transfer is complete or not.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */

int32_t lte_ifc_fota_get_state(uint8_t* p_state,
                               uint8_t* p_end_reason,
                               uint32_t* p_num_blocks_received,
                               uint32_t* p_num_requests_sent,
                               bool* p_transfer_complete);


/**
 * @brief
 *   Adds an ASCII message to the debug log.
 *
 * @param[in] chars
 *   ASCII chars that make up the 'message'. Only printable characters
 *   are accepted: must be in range (0x20 <= x <= 0x7E).
 *
 * @param[in] len
 *   Length of the message (number of ASCII chars). This is needed since the
 *   message argument is not NULL-terminated. Max length is 32 chars.
 *
 * @return
 *   @ref lte_ifc_ack_code
 */
int32_t lte_ifc_debug_msg(uint8_t *chars, uint16_t len);

/**
* @brief
*   Set how often the module checks for the availability of a FOTA
*   image.
*
* @param[in] period
*   Check period, in minutes. All uint16_t values are allowed:
*   0 = FOTA check is disabled
*   1-65535 = FOTA check every 1 to 65,535 minutes.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_fota_check_period_set(uint16_t period);

/**
* @brief
*   Get fota check period.
*
* @param[out] p_period
*   See set command for parameter details.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_fota_check_period_get(uint16_t *p_period);

/**
* @brief
*   Set when the module checks for the availability of a FOTA
*   image.
*
* @param[in] check_at_boot
*   Should a check be performed when the Link Labs MCU is reset?
*   0 = No
*   1 = Yes
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_fota_check_at_boot_set(uint8_t check_at_boot);

/**
* @brief
*   Get when the module checks for the availability of a FOTA image.
*
* @param[out] p_check_at_boot
*   See set command for parameter details.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_fota_check_at_boot_get(uint8_t *p_check_at_boot);

/**
* @brief
*   Set the module's power policy.
*
* @param[in] pwr_policy
*   0 = On/Off with inactivity timer
*   1 = LTE Cat-M1 Power Savings Mode (PSM)
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_pwr_policy_set(uint8_t pwr_policy);

/**
* @brief
*   Get the module's power policy.
*
* @param[out] p_pwr_policy
*   See set command for parameter details.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_pwr_policy_get(uint8_t *p_pwr_policy);

/**
* @brief
*   Get the latest snapshot of the LTE Network Info data.
*
* @details
*   The module periodically obtains a structure of data fields known as the "Network Info".
*   This structure contains all of the arguments of this function. Calling this
*   function writes out the most up-to-date values of these fields. The info is updated
*   any time the module interacts with the network (e.g. when sending uplink messages
*   or checking its mailbox). If an update of the network info is desired, it can be
*   requested using the lte_ifc_network_info_update() function, which will perform
*   just the update and not send or received any data.
*
* @param[out] p_u32_time_utc_last_rx_s
*   The UTC time in seconds since the Unix epoch (e.g. seconds since 00:00 hours, Jan 1, 1970 UTC).
*   This value is not the current time but instead the exact last time that was provided from the
*   network. For example, if the last network attach was 45 seconds ago, then this time will
*   differ from the current time by 45 seconds.
*
* @param[out] p_u32_time_utc_now_s
*   An estimate of the current UTC time now. This time is computed by taking the last
*   p_u32_time_utc_last_rx_s and adding to it an estimate of how much time has elapsed since that
*   time was provided by the network. Note that since the module does not have an accurate clock,
*   there can be significant error in this value if that last network attach was a long time ago.
*   If accurate time is needed, it is recommended to first request a network time update (either by
*   performing an uplink or downlink transaction or by calling lte_ifc_network_info_update()),
*   and then waiting for that transaction to complete succesfully (using lte_ifc_message_status()),
*   and then calling this function.
*
* @param[out] p_i8_ofst_qhrs
*   The difference between UTC time and local time, in quarter-hour units. For example, a value of
*   20 corresponds to 5 hours difference between UTC and local.
*
* @param[out] p_i16_rsrp_dbm
*   The Reference Signal Received Power (RSRP) in dBm.
*
* @param[out] p_i16_rsrq_db
*   The Reference Signal Received Quality (RSRQ) in dB.
*
* @param[out] p_u32_cell_id
*   The 4-byte E-UTRAN Cell Id of the cell that the message was sent through.
*
* @param[out] p_u16_cell_tac
*   The Tracking Area Code of the cell that the message was sent through.
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_network_info_get(uint32_t* p_u32_time_utc_last_rx_s,
                                 uint32_t* p_u32_time_utc_now_s,
                                 int8_t* p_i8_ofst_qhrs,
                                 int16_t*  p_i16_rsrp_dbm,
                                 int16_t*  p_i16_rsrq_db,
                                 uint32_t* p_u32_cell_id,
                                 uint16_t* p_u16_cell_tac);

/**
* @brief
*   Command the module to search for a cell tower and update the LTE Network Info data.
*
* @param[out] p_request_id
*   A request id, which can be used to query the result of the specific update request
*   (e.g. success or failure).
*
* @return
*   @ref lte_ifc_ack_code
*   */
int32_t lte_ifc_network_info_update(uint32_t* p_request_id);


/** @addtogroup lte_ifc_ack_code lte_ifc_ack_code
 *  @{
 */

/**
 * @brief
 *   A short string providing human readable text describing an ack code.
 *
 * @param[in] ack_code
 *   The ack code.
 *
 * @return
 *   The string description.
 */
const char* lte_ifc_ack_desc(int32_t ack_code);

/** @} (end addtogroup lte_ifc_ack_code) */

/** @addtogroup lte_ifc_irq_flags lte_ifc_irq_flags
 *  @{
 */

/**
 * @brief
 *   A short string providing human readable text describing an irq flag. This
 *   function should usually be wrapped by pseudo code similar to: <br>
 *   // Print out strings corresponding to the bits set in irq_flags_vec <br>
 *   for uint32_t n = 0 to 31 {<br> 
 *   &nbsp;&nbsp; single_bit = 1 << n <br>
 *   &nbsp;&nbsp; if (irq_flags_vec & single_bit) {PRINT("%s ", lte_ifc_irq_flag_desc(single_bit))} <br>
 *   } <br>
 *
 * @param[in] irq_flag_bit
 *    The irq flag bit that you want a string representation of. This 
 *    parameter must have only one bit set, since only one string is
 *    be returned per call. 
 *
 * @return
 *   The string description.
 */
const char* lte_ifc_irq_flag_desc(uint32_t irq_flag_bit);

/**
 * @brief
 *   Read (and optionally clear) IRQ flags in module.
 *
 * @details
 *   This function allows the external host to check whether an event has occurred in the
 *   module that has latched a bit in the "IRQ Flags" vector.
 *
 * @param[in] flags_to_clear
 *   A uint32_t bit vector containing flags that should be cleared if they are set. This can be
 *   0 if the host interface just wants to read without clearing. If a bit is set, this function
 *   performs a clear-on-read of the irq_flags bits passed in.
 *
 * @param[out] flags
 *   A uint32_t bit vector - the value of the irq_flags in the module. Note that if the flags_to_clear
 *   argument is non-zero, this argument is the value of the flags before the clear operation.
 *
 * @return
 *   0 - success, negative otherwise.
 */
int32_t lte_ifc_irq_flags(uint32_t flags_to_clear, uint32_t *flags);

/** @} (end addtogroup lte_ifc_irq_flags) */

/** @} (end addtogroup lte_ifc_common) */

/** @} (end addtogroup Link_Labs_LTE_Interface_Library) */

#ifdef __cplusplus
}
#endif

#endif /* __LTE_IFC_H */
