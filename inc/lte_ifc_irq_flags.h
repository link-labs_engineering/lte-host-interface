#ifndef __LTE_IFC_IRQ_FLAGS_H__H__
#define __LTE_IFC_IRQ_FLAGS_H__H__

/**
 * @addtogroup lte_ifc_irq_flags lte_ifc_irq_flags
 *
 * @brief A vector of bits each indicating an event code.
 *
 * An irq_flag is a flag that is set in the irq_flags_vector, which is a 32-bit value.
 * Each bit indicates a specific event that has occured, which is intended to
 * be communicated from the LTE-M Module to the host processor. Irq flags can only
 * be set by the LTE-M Module, and can be cleared by the host processor (through a
 * host interface command).
 *
 * Anytime a bit is set in the irq_flags_vector, the irq_flags GPIO line is set. The GPIO
 * line is cleared when all bits in the irq_flags vector are cleared. The GPIO line is
 * intended to be used to wake up a sleeping host processor, through the use of an
 * edge-triggered GPIO interrupts.
 *
 * A descriptive text string for any error code can be obtained at runtime by calling the
 * function @ref lte_ifc_irq_flag_desc.
 *
 * @{
 */


//#define LTE_IRQ_FLAGS_RESET                   (0x00000001UL)  ///< Set every time the module reboots for any reason
#define LTE_IRQ_FLAGS_ASSERT                  (0x00000002UL)  ///< Set every time an assert occured (after rebooting)
#define LTE_IRQ_FLAGS_WDOG                    (0x00000004UL)  ///< Set every time a hw watchdog reboot occurs (after rebooting)
#define LTE_IRQ_FLAGS_TX_DONE                 (0x00000010UL)  ///< Set every time a Tx Queue goes from non-empty to empty
#define LTE_IRQ_FLAGS_TX_STATUS_QUEUE_FULL    (0x00000020UL)  ///< Set if the Tx Status queue has filled. Call lte_ifc_message_status to empty it.
#define LTE_IRQ_FLAGS_RX_DONE                 (0x00000100UL)  ///< Set every time a new mailbox message is available.
#define LTE_IRQ_FLAGS_RX_QUEUE_FULL           (0x00000200UL)  ///< Set if the donwlink queue has filled. Call lte_ifc_retrieve_message to empty it.
#define LTE_IRQ_FLAGS_RX_ERROR                (0x00000400UL)  ///< Set if an error occurs while expecting a new downlink message.

/** @} (end addtogroup lte_ifc_irq_flags) */

// When adding a new value, update lte_ifc_irq_flag_desc in lte_ifc.c
// When adding a new value, update vector in lte_ifc-py


#endif // __LTE_IFC_IRQ_FLAGS_H__H__

