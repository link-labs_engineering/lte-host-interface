#ifndef __LTE_IFC_HAL_H__
#define __LTE_IFC_HAL_H__

#include <stdbool.h>
#include <stdint.h>

/**
* @addtogroup Link_Labs_LTE_Interface_Library lte_ifc
* @{
*/

/** @addtogroup lte_ifc_hal lte_ifc_hal
 *  @{
 */
    /**
     * @brief Get wake status
     *
     * @param[out] p_wake
     *   Set true if module is awake, false otherwise.
     *
     * @return
     *   0 - success, negative otherwise.
     */
    int32_t lte_ifc_hal_get_wake_status(bool* p_wake);

    /**
     * @brief Get IRQ Flags GPIO state
     *
     * @param[out] p_level
     *   Set true if line is high, false otherwise.
     *
     * @return
     *   0 - success, negative otherwise.
     */
    int32_t lte_ifc_hal_get_irq_flags_gpio(bool* p_level);


    struct lte_ifc_time
    {
        uint32_t tv_sec;
        uint32_t tv_nsec;
    };

    /**
     * @brief
     *   Get the current time. This time need not be a UTC or localtime. It is
     *   used only for checking timeouts, and only relative to repeated calls of this
     *   function. Thus, an uptime counter is usually sufficient to implement 
     *   this function on systems which do not have access to UTC or localtime.
     *
     * @param[out] tp
     *   This address will be updated with the current value of the time fields.
     *
     * @return
     *   0 - success, negative otherwise.
     */
    int32_t lte_ifc_hal_gettime(struct lte_ifc_time *tp);
    
    /**
    * @brief
    * Read the next byte (if available) from the Link Labs module. This is expected
    *  to be a non-blocking function. If a new byte is available, it should be stored in
    *  *buff and the return argument set to zero. If there is no byte available, the function
    *  should return immediately with a return argument of any negative number.
    *
    * @param[out] buff
    *   The byte read (if available).
    *
    * @return
    *   0 - success, negative otherwise.
    */
   int32_t lte_ifc_hal_read_byte(uint8_t *buff);

   /**
    * @brief Set wake request
    *
    * @param[in] wake
    *  True if request module to wake, false to allow module to sleep.
    *
    * @return
    *   0 - success, negative otherwise.
    */
   int32_t lte_ifc_hal_set_wake_request(bool wake);

    /**
     * @brief Write data to the Link Labs module. This function can either be a blocking call (does not
     * return until all bytes are transmitted) or a non-blocking call (returns immediately after queuing
     * bytes). Note that in the non-blocking case the bytes in *buff must be copied out, as they are not
     * held constant after the function returns.
     *
     * @param[in] buff
     *   The buffer containing the data to write to the module.  The size of
     *   buff must be at least len bytes.
     *
     * @param[in] len
     *   The number of bytes to write.
     *
     * @return
     *   0 - success, negative otherwise.
     *
     * This function is usually a simple UART wrapper.
     */
    int32_t lte_ifc_hal_write(uint8_t *buff, uint16_t len);


    /** @} (end addtogroup lte_ifc_hal) */
    /** @} (end addtogroup Link_Labs_LTE_Interface_Library) */

#endif // __LTE_IFC_HAL_H__

