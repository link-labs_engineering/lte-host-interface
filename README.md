# Link Labs LTE Interface library  
The Link Labs LTE Interface library (lte_ifc) allows an external host processor to communicate with the Link Labs cloud server (Conductor) using a Link Labs LTE Cat-M1 module. This module implements the LTE power management, IP communication, Security and Encryption, and other features in order to simplify the responsibilities of the host processor.

This library combined with a Link Labs module allows software developers to quickly and easily add LTE Cat-M1 networking to products. The library runs on an external host processor which can be nearly any microcontroller or PC with a UART interface. The library is written in C. The API documentation can be found here:  

[http://doxygen.link-labs.com/ltem/doxygen/index.html](http://doxygen.link-labs.com/ltem/doxygen/index.html)


