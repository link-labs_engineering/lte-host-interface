/**
\mainpage Introduction

The Link Labs LTE Interface library (lte_ifc) allows an external host processor to communicate with the Link Labs cloud server (Conductor) using a Link Labs LTE Cat-M1 module.  This module implements the LTE power management, IP communication, Security and Encryption, and other features in order to simplify the responsibilities of the host processor.

This documentation describes how an external host should use the library. The documentation is intended for software developers responsible for incorporating this networking into their products.  This library combined with a 
Link Labs module allows software developers to quickly and easily add LTE Cat-M1 networking to products.  

Next: \ref Link_Labs_LTE_Interface_Library

*/
