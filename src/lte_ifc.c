#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "lte_ifc.h"
#include "lte_ifc_bytedefs.h"
#include "lte_ifc_irq_flags.h"
#include "lte_ifc_opcodes.h"

// If PC only
#if defined(WIN32) || defined(__MINGW32__) || defined(__APPLE__) || defined(__linux__)
    #include "lte_ifc_hal.h"
#endif

#ifndef NULL    // <time.h> defines NULL on *some* platforms
#define NULL                (0)
#endif

static uint16_t compute_checksum(uint8_t *hdr, uint16_t hdr_len, uint8_t *payload, uint16_t payload_len);
static int32_t send_packet(opcode_t op, uint8_t ver, uint8_t message_num, uint8_t *buf, uint16_t len);
static int32_t recv_packet(opcode_t op, uint8_t message_num, uint8_t *buf, uint16_t len);

static int32_t message_num = 0;

static int32_t hal_read_write(opcode_t op, uint8_t ver, uint8_t buf_in[], uint16_t in_len, uint8_t buf_out[], uint16_t out_len)
{
    int32_t ret;

    // Error checking:
    // Only valid combinations of buffer & length pairs are:
    // buf == NULL, len = 0
    // buf != NULL, len > 0
    if (((buf_in  != NULL) && ( in_len == 0)) || (( buf_in == NULL) && ( in_len > 0)))
    {
        return(LTE_IFC_ACK_CODE_INCORRECT_PARAMETER);
    }
    if (((buf_out != NULL) && (out_len == 0)) || ((buf_out == NULL) && (out_len > 0)))
    {
        return(LTE_IFC_ACK_CODE_INCORRECT_PARAMETER);
    }
    
    // OK, inputs have been sanitized. Carry on...
    ret = send_packet(op, ver, message_num, buf_in, in_len);
//    printf("\t send packet returns %d\n", ret);
    if (ret == 0)
    {
        ret = recv_packet(op, message_num, buf_out, out_len);
//        printf("\t recv packet returns %d\n", ret);
    }
    
    // Let module go back to sleep (if it chooses to) now that packet has been sent
    if(lte_ifc_hal_set_wake_request(false) != 0)
    {
        return LTE_IFC_ACK_CODE_WAKEUP_SET;
    }

    // if we got an error, flush the rx buffer
    uint8_t dummy_byte;
    while (lte_ifc_hal_read_byte(&dummy_byte) == 0)
    {
        // printf("\t\t flushed byte\n");
    };

    message_num++;

    return(ret);
}


char const * lte_ifc_ack_desc(int32_t return_code)
{
    switch (return_code)
    {
        // 0
        case LTE_IFC_ACK:                           return "Success";

        // -1 to -99
        case LTE_IFC_ACK_CODE_CMD_NOT_SUPPORTED:        return "Command not supported";
        case LTE_IFC_ACK_CODE_INCORRECT_CHKSUM:         return "Incorrect Checksum";
        case LTE_IFC_ACK_CODE_PAYLOAD_LEN_OOR:          return "Length of payload sent in command was out of range";
        case LTE_IFC_ACK_CODE_PAYLOAD_OOR:              return "Payload sent in command was out of range";
        case LTE_IFC_ACK_CODE_BOOTUP_IN_PROGRESS:       return "Not allowed since firmware bootup still in progress";
        case LTE_IFC_ACK_CODE_BUSY_TRY_AGAIN:           return "Temporarily unavailable";
        case LTE_IFC_ACK_CODE_PAYLOAD_LEN_EXCEEDED:     return "Payload length is greater than the max supported length";
        case LTE_IFC_ACK_CODE_NOT_IN_MAILBOX_MODE:      return "Module must be in DOWNLINK_MAILBOX mod";
        case LTE_IFC_ACK_CODE_PAYLOAD_BAD_PROPERTY:     return "Invalid property specified in command";
        case LTE_IFC_ACK_CODE_NODATA:                   return "No data is available to be returned";
        case LTE_IFC_ACK_CODE_CMD_VER_NOT_SUPPORTED:    return "Command version not supported";
        case LTE_IFC_ACK_CODE_OTHER:                    return "Other";

        // -101 to -200
        case LTE_IFC_ACK_CODE_LEN_OOR:                  return "SQN_ERR_LEN_OOR";
        case LTE_IFC_ACK_CODE_QUEUE_FULL_BYTES:         return "SQN_ERR_QUEUE_FULL_BYTES";
        case LTE_IFC_ACK_CODE_QUEUE_FULL_NUM:           return "SQN_ERR_QUEUE_FULL_NUM";
        case LTE_IFC_ACK_CODE_CALLER_PRIO_TOO_HIGH:     return "SQN_ERR_CALLER_PRIO_TOO_HIGH";
        case LTE_IFC_ACK_CODE_PDP_TIMEOUT:              return "SQN_PDP_TIMEOUT";
        case LTE_IFC_ACK_CODE_REG_TIMEOUT:              return "SQN_REG_TIMEOUT";
        case LTE_IFC_ACK_CODE_CONNECT_TIMEOUT:          return "SQN_CONNECT_TIMEOUT";
        case LTE_IFC_ACK_CODE_TX_TIMEOUT:               return "SQN_TX_TIMEOUT";
        case LTE_IFC_ACK_CODE_HANGUP_IN_DIAL:           return "SQN_HANGUP_IN_DIAL";
        case LTE_IFC_ACK_CODE_ERR_MUTEX:                return "SQN_ERR_MUTEX";
        case LTE_IFC_ACK_CODE_HANGUP_IN_SOCK_OPEN:      return "SQN_HANGUP_IN_SOCK_OPEN";
        case LTE_IFC_ACK_CODE_SEND_WITHOUT_CONN:        return "SQN_SEND_WITHOUT_CONN";
        case LTE_IFC_ACK_CODE_SEND_DURING_LISTEN:       return "SQN_SEND_DURING_LISTEN";
        case LTE_IFC_ACK_CODE_FOTA_IN_PROGRESS:         return "SQN_FOTA_IN_PROGRESS";
        case LTE_IFC_ACK_CODE_RX_TIMEOUT:               return "SQN_RX_TIMEOUT";
        case LTE_IFC_ACK_CODE_TIMEOUT_SERVER_RSP:       return "SQN_TIMEOUT_SERVER_RSP";
        case LTE_IFC_ACK_CODE_ERR_SERVER_RSP_ERR:       return "SQN_ERR_SERVER_RSP_ERR";
        case LTE_IFC_ACK_CODE_ERR_SERVER_RSP_CODE:      return "SQN_ERR_SERVER_RSP_CODE";
        case LTE_IFC_ACK_CODE_IFC_QUEUE_PUSH_FAIL:      return "SQN_IFC_QUEUE_PUSH_FAIL";
        case LTE_IFC_ACK_CODE_ERR_UNKNOWN:              return "SQN_ERR_UNKNOWN";
        case LTE_IFC_ACK_CODE_ERR_NOT_REGISTERED:       return "SQN_ERR_NOT_REGISTERED";
        case LTE_IFC_ACK_CODE_ERR_INIT_IN_PROGRESS:     return "SQN_ERR_INIT_IN_PROGRESS";

        // -201 to -300
        case LTE_IFC_ACK_CODE_INCORRECT_PARAMETER:       return "INCORRECT_PARAMETER";
        case LTE_IFC_ACK_CODE_INCORRECT_RESPONSE_LENGTH: return "INCORRECT_RESPONSE_LENGTH";
        case LTE_IFC_ACK_CODE_MESSAGE_NUMBER_MISMATCH:   return "MESSAGE_NUMBER_MISMATCH";
        case LTE_IFC_ACK_CODE_CHECKSUM_MISMATCH:         return "CHECKSUM_MISMATCH";
        case LTE_IFC_ACK_CODE_COMMAND_MISMATCH:          return "COMMAND_MISMATCH";
        case LTE_IFC_ACK_CODE_HOST_INTERFACE_TIMEOUT:    return "HOST_INTERFACE_TIMEOUT";
        case LTE_IFC_ACK_CODE_BUFFER_TOO_SMALL:          return "BUFFER_TOO_SMALL";
        case LTE_IFC_ACK_CODE_START_OF_FRAME:            return "START_OF_FRAME";
        case LTE_IFC_ACK_CODE_HEADER:                    return "HEADER";
        case LTE_IFC_ACK_CODE_TIMEOUT:                   return "TIMEOUT";
        case LTE_IFC_ACK_CODE_INCORRECT_MESSAGE_SIZE:    return "INCORRECT_MESSAGE_SIZE";
        case LTE_IFC_ACK_CODE_NO_NETWORK:                return "NO_NETWORK";
        case LTE_IFC_ACK_CODE_WAKEUP_TIMEOUT:            return "TIMEOUT_WAKEUP";
        case LTE_IFC_ACK_CODE_WAKEUP_GET:                return "TIMEOUT_WAKEUP_GET";
        case LTE_IFC_ACK_CODE_WAKEUP_SET:                return "TIMEOUT_WAKEUP_SET";
        case LTE_IFC_ACK_CODE_IRQFLAGS_GPIO_GET:         return "IRQFLAGS_GPIO_GET";
        case LTE_IFC_ACK_CODE_WRITE:                     return "HAL_WRITE_ERR";
        case LTE_IFC_ACK_CODE_GETTIME:                   return "HAL_GETTIME";
        default:
            return "UNKNOWN";
    }
}

char const * lte_ifc_irq_flag_desc(uint32_t irq_flag_bit)
{
    char const* s = "UNKNOWN";
    switch (irq_flag_bit)
    {
        case LTE_IRQ_FLAGS_ASSERT:
            s = "FATAL ERROR";
            break;
        case LTE_IRQ_FLAGS_WDOG:
            s = "WATCHDOG";
            break;
        case LTE_IRQ_FLAGS_TX_DONE:
            s = "TX DONE";
            break;
        case LTE_IRQ_FLAGS_TX_STATUS_QUEUE_FULL:
            s = "TX STATUS QUEUE FULL";
            break;
        case LTE_IRQ_FLAGS_RX_DONE:
            s = "RX DONE";
            break;
        case LTE_IRQ_FLAGS_RX_QUEUE_FULL:
            s = "RX QUEUE FULL";
            break;
    }
    return s;
}

static inline void _lte_ifc_ordered_memcpy(uint8_t* p_dest, const uint8_t* p_src, size_t len, bool reverse_order)
{
    // Copy byte wise in case output is an unaligned address
    uint8_t n_dest;
    uint8_t n_src = reverse_order ? len-1 : 0;

    for(n_dest=0; n_dest<len; n_dest++)
    {
        p_dest[n_dest] =  p_src[n_src];

        if (reverse_order)
        {
            n_src--;
        }
        else
        {
            n_src++;
        }
    }
}

static uint32_t _lte_ifc_deserialize_u32_big(uint8_t const * p_in)
{
    uint32_t x = 0;
    _lte_ifc_ordered_memcpy((uint8_t*)&x, p_in, sizeof(uint32_t), true);
    return x;
}

static uint16_t _lte_ifc_deserialize_u16_big(uint8_t const* p_in)
{
    uint16_t x = 0;
    _lte_ifc_ordered_memcpy((uint8_t*)&x, p_in, sizeof(uint16_t), true);
    return x;
}

int32_t lte_ifc_version_get(lte_version_t *version)
{
    uint8_t buf[VERSION_LEN];
    int32_t ret;
    if(NULL == version)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    ret = hal_read_write(OP_VERSION, 0, NULL, 0, buf, VERSION_LEN);
    if(ret < 0)
    {
        return ret;
    }

    if (VERSION_LEN != ret)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_RESPONSE_LENGTH;
    }

    version->major = buf[0];
    version->minor = buf[1];
    version->tag = buf[2] << 8 | buf[3];

    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_imei_get(uint64_t* p_imei)
{
    int32_t ret;

    if (p_imei == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    uint8_t buf[8];


    ret = hal_read_write(OP_IMEI_GET, 0, NULL, 0, &buf[0], 8);
    if(ret < 0)
    {
        return ret;
    }

    if (sizeof(uint64_t) != ret)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_RESPONSE_LENGTH;
    }

    *p_imei  = ((uint64_t)buf[0]) << 56;
    *p_imei |= ((uint64_t)buf[1]) << 48;
    *p_imei |= ((uint64_t)buf[2]) << 40;
    *p_imei |= ((uint64_t)buf[3]) << 32;
    *p_imei |= ((uint64_t)buf[4]) << 24;
    *p_imei |= ((uint64_t)buf[5]) << 16;
    *p_imei |= ((uint64_t)buf[6]) << 8;
    *p_imei |= ((uint64_t)buf[7]);

    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_settings_save(void)
{
    int32_t ret = hal_read_write(OP_SETTINGS_SAVE, 0, NULL, 0, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_settings_delete(void)
{
    int32_t ret = hal_read_write(OP_SETTINGS_DELETE, 0, NULL, 0, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_reset_mcu(void)
{
    uint8_t force = 0;
    int32_t ret = hal_read_write(OP_RESET_MCU, 1, &force, 1, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_reset_mcu_force(void)
{
    uint8_t force = 1;
    int32_t ret = hal_read_write(OP_RESET_MCU, 1, &force, 1, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_bootloader_mode(void)
{
    int32_t ret = hal_read_write(OP_TRIGGER_BOOTLOADER, 0, NULL, 0, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

/**
 * @return
 *   0 - success
 *   negative = error, as defined by hal_read_write
 */
int32_t lte_ifc_irq_flags(uint32_t flags_to_clear, uint32_t *flags)
{
    // Check the GPIO line, polling might not be needed
    bool irq_flags_gpio_set = false;
    int32_t i32_ret = lte_ifc_hal_get_irq_flags_gpio(&irq_flags_gpio_set);
    if (i32_ret != 0)
    {
        return LTE_IFC_ACK_CODE_IRQFLAGS_GPIO_GET;
    }
    if (irq_flags_gpio_set == false)
    {
        *flags = 0;
        return LTE_IFC_ACK;
    }
    
    if(NULL == flags)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    // Assuming big endian convention over the interface
    uint8_t in_buf[4];
    uint8_t out_buf[4] = {0,0,0,0};

    in_buf[0] = (uint8_t)((flags_to_clear >> 24) & 0xFF);
    in_buf[1] = (uint8_t)((flags_to_clear >> 16) & 0xFF);
    in_buf[2] = (uint8_t)((flags_to_clear >>  8) & 0xFF);
    in_buf[3] = (uint8_t)((flags_to_clear      ) & 0xFF);

    int32_t ret = hal_read_write(OP_IRQ_FLAGS, 0, in_buf, 4, out_buf, 4);

    if(ret < 0)
    {
        return ret;
    }

    if(4 != ret)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_RESPONSE_LENGTH;
    }

    uint32_t flags_temp = 0;
    flags_temp |= (((uint32_t)out_buf[0]) << 24);
    flags_temp |= (((uint32_t)out_buf[1]) << 16);
    flags_temp |= (((uint32_t)out_buf[2]) << 8);
    flags_temp |= (((uint32_t)out_buf[3]));
    *flags = flags_temp;

    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_get_assert_info(char *filename, uint32_t* p_line, uint32_t* p_uptime_sec)
{
    // Set outputs to default values. Overwrite them if we get a response from module.
    *filename = '\0';
    *p_line = 0;
    *p_uptime_sec = 0;

    uint8_t tmp_arr[28];
    int32_t ret = hal_read_write(OP_ASSERT_GET, 0, NULL, 0, tmp_arr, 28);
    if (ret == 28)
    {
        // Get filename
        memcpy(filename, tmp_arr, 20);

        // Get line number
        *p_line = 0;
        *p_line += (uint32_t)(tmp_arr[20]) << 24;
        *p_line += (uint32_t)(tmp_arr[21]) << 16;
        *p_line += (uint32_t)(tmp_arr[22]) <<  8;
        *p_line += (uint32_t)(tmp_arr[23]) <<  0;

        *p_uptime_sec = 0;
        *p_uptime_sec += (uint32_t)(tmp_arr[24]) << 24;
        *p_uptime_sec += (uint32_t)(tmp_arr[25]) << 16;
        *p_uptime_sec += (uint32_t)(tmp_arr[26]) <<  8;
        *p_uptime_sec += (uint32_t)(tmp_arr[27]) <<  0;
        return LTE_IFC_ACK;
    }
    else
    {
        return LTE_IFC_NACK_NODATA;
    }
}

int32_t lte_ifc_trigger_assert(void)
{
    return hal_read_write(OP_ASSERT_SET, 0, NULL, 0, NULL, 0);
}

int32_t lte_ifc_receive_mode_set(uint8_t rx_mode)
{
    if (rx_mode >= NUM_DOWNLINK_MODES)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    int32_t ret = hal_read_write(OP_RX_MODE_SET, 0, &rx_mode, 1, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_receive_mode_get(uint8_t *rx_mode)
{
    if (rx_mode == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    int32_t ret = hal_read_write(OP_RX_MODE_GET, 0, NULL, 0, rx_mode, sizeof(*rx_mode));
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_message_send(uint8_t buf[], uint16_t len, uint8_t port, uint32_t* p_packet_id)
{
    (void)port;

    if (buf == NULL || len <= 0 || len > MAX_USER_UPLINK_LENGTH_BYTES || p_packet_id == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    uint8_t temp[4] = {0};

    int32_t ret = hal_read_write(OP_UPLINK_SEND, 0, buf, len, temp, 4);
    if (ret > 0)
    {
        (*p_packet_id) = 0;
        (*p_packet_id) += ((uint32_t)(temp[3])) <<  0;
        (*p_packet_id) += ((uint32_t)(temp[2])) <<  8;
        (*p_packet_id) += ((uint32_t)(temp[1])) << 16;
        (*p_packet_id) += ((uint32_t)(temp[0])) << 24;
        return LTE_IFC_ACK;
    }
    else
    {
        return ret;
    }
}

int32_t lte_ifc_message_status(uint8_t* p_new_status, uint32_t* p_id, int32_t* p_status, uint32_t* p_time_in_queue_ms)
{
    if ((p_new_status == NULL) || (p_id == NULL) || (p_status == NULL) || (p_time_in_queue_ms == NULL))
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    uint8_t temp[9] = {0};

    int32_t ret = hal_read_write(OP_UPLINK_STATUS, 0, NULL, 0, temp, 9);
    if (ret == 9)
    {
        *p_new_status = 1;
        *p_id = _lte_ifc_deserialize_u32_big((const uint8_t*)&temp[0]);
        *p_status = (int32_t)-temp[4];
        *p_time_in_queue_ms = _lte_ifc_deserialize_u32_big((const uint8_t*)&temp[5]);

        return LTE_IFC_ACK;
    }
    else
    {
        (*p_new_status) = 0;
        return ret;
    }

}

int32_t lte_ifc_message_status_ext(uint8_t* p_new_status, uint32_t* p_id, int32_t* p_status, uint32_t* p_time_in_queue_ms,
                                   int16_t* p_i16_rsrq_db, int16_t* p_i16_rsrp_dbm, uint32_t* p_u32_cell_id, uint16_t* p_u16_cell_tac)
{
    if ((p_new_status == NULL) || (p_id == NULL) || (p_status == NULL) || (p_time_in_queue_ms == NULL))
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    if ((p_i16_rsrq_db == NULL) || (p_i16_rsrp_dbm == NULL) || (p_u32_cell_id == NULL) || (p_u16_cell_tac == NULL))
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    uint8_t temp[19] = {0};

    int32_t ret = hal_read_write(OP_UPLINK_STATUS, 1, NULL, 0, temp, 19);
    if (ret == 19)
    {
        *p_new_status = 1;
        *p_id = _lte_ifc_deserialize_u32_big((const uint8_t*)&temp[0]);
        *p_status = (int32_t)-temp[4];
        *p_time_in_queue_ms = _lte_ifc_deserialize_u32_big((const uint8_t*)&temp[5]);

        *p_i16_rsrq_db = _lte_ifc_deserialize_u16_big((const uint8_t*)&temp[9]);
        *p_i16_rsrp_dbm = _lte_ifc_deserialize_u16_big((const uint8_t*)&temp[11]);
        *p_u32_cell_id = _lte_ifc_deserialize_u32_big((const uint8_t*)&temp[13]);
        *p_u16_cell_tac = _lte_ifc_deserialize_u16_big((const uint8_t*)&temp[17]);

        return LTE_IFC_ACK;
    }
    else
    {
        (*p_new_status) = 0;
        return ret;
    }

}

int32_t lte_ifc_mailbox_request(uint32_t* p_packet_id)
{

    if (p_packet_id == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    uint8_t temp[4] = {0};
    int32_t ret = hal_read_write(OP_MAILBOX_REQUEST, 0, NULL, 0, temp, 4);
    if (ret > 0)
    {
        (*p_packet_id) = 0;
        (*p_packet_id) += ((uint32_t)(temp[3])) <<  0;
        (*p_packet_id) += ((uint32_t)(temp[2])) <<  8;
        (*p_packet_id) += ((uint32_t)(temp[1])) << 16;
        (*p_packet_id) += ((uint32_t)(temp[0])) << 24;
        return LTE_IFC_ACK;
    }
    else
    {
        return ret;
    }
}

int32_t lte_ifc_retrieve_message(uint8_t *buf, uint16_t *size, uint8_t *port)
{
    int32_t rw_response;
    uint16_t size_in = *size;
    *size = 0;

    if ((buf == NULL) || (size == NULL))
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    rw_response = hal_read_write(OP_MSG_RECV, 0, NULL, 0, buf, size_in);

    if (rw_response < 0)
    {
        return rw_response;
    }
    else if (rw_response == 0)
    {
        return LTE_IFC_ACK;
    }
    if (rw_response == 1)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_RESPONSE_LENGTH;
    }
    else
    {
        *size = rw_response-1;
    }

    // Port can be null if you dont care
    if (NULL != port)
    {
        *port = buf[0];
    }

    // get rid of port in buffer
    memmove(buf, buf + 1, *size);

    return LTE_IFC_ACK;
}

int32_t lte_ifc_fota_download(uint8_t *buf, uint16_t len)
{

    if ((buf == NULL) || (len <= 0))
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    int32_t ret = hal_read_write(OP_TEST_FOTA_DL, 0, buf, len, NULL, 0);
    if (ret > 0)
    {
        return LTE_IFC_ACK;
    }
    else
    {
        return ret;
    }

    return LTE_IFC_ACK;
}

int32_t lte_ifc_fota_get_state(uint8_t* p_state,
                               uint8_t* p_end_reason,
                               uint32_t* p_num_blocks_received,
                               uint32_t* p_num_requests_sent,
                               bool* p_transfer_complete)
{
    if ((p_state == NULL) ||
        (p_end_reason == NULL) ||
        (p_num_blocks_received == NULL) ||
        (p_num_requests_sent == NULL) ||
        (p_transfer_complete == NULL))

    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    uint8_t buf[11];
    int32_t ret = hal_read_write(OP_TEST_FOTA_GET_STATE, 0, NULL, 0, buf, 11);
    if (ret > 0)
    {
        *p_state = (uint8_t) buf[0];
        *p_end_reason = (uint8_t) buf[1];

        *p_num_blocks_received  = (uint32_t) buf[2] << 24;
        *p_num_blocks_received += (uint32_t) buf[3] << 16;
        *p_num_blocks_received += (uint32_t) buf[4] << 8;
        *p_num_blocks_received += (uint32_t) buf[5];

        *p_num_requests_sent  = (uint32_t) buf[6] << 24;
        *p_num_requests_sent += (uint32_t) buf[7] << 16;
        *p_num_requests_sent += (uint32_t) buf[8] << 8;
        *p_num_requests_sent += (uint32_t) buf[9];

        *p_transfer_complete = (uint8_t) buf[10];
        return LTE_IFC_ACK;
    }
    else
    {
        return ret;
    }

    return LTE_IFC_ACK;

}

int32_t lte_ifc_debug_msg(uint8_t *chars, uint16_t len)
{
    if ((chars == NULL) || (len <= 0) || (len > 32))
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    int32_t n;
    for (n=0; n<len; n++)
    {
        if ((chars[n] < 0x20) || (chars[n] > 0x7e))
        {
            return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
        }
    }

    int32_t ret = hal_read_write(OP_DEBUG_MSG, 0, chars, len, NULL, 0);
    if (ret > 0)
    {
        return LTE_IFC_ACK;
    }
    else
    {
        return ret;
    }

    return LTE_IFC_ACK;
}

int32_t lte_ifc_fota_check_period_set(uint16_t period)
{
    // Assuming big endian convention over the interface
    uint8_t buf[2];
    buf[0] = (uint8_t)((period >> 8) & 0xFF);
    buf[1] = (uint8_t)((period     ) & 0xFF);

    int32_t ret = hal_read_write(OP_FOTA_PERIOD_SET, 0, (uint8_t*)&buf[0], 2, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_fota_check_period_get(uint16_t *p_period)
{
    if (p_period == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    uint8_t rsp_buf[2] = {0,0};
    int32_t ret = hal_read_write(OP_FOTA_PERIOD_GET, 0, NULL, 0, rsp_buf, 2);

    *p_period =  rsp_buf[1];
    *p_period += ((uint16_t)rsp_buf[0]) << 8;
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_fota_check_at_boot_set(uint8_t check_at_boot)
{
    int32_t ret = hal_read_write(OP_FOTA_CHECK_AT_BOOT_SET, 0, &check_at_boot, 1, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_fota_check_at_boot_get(uint8_t *p_check_at_boot)
{
    if (p_check_at_boot == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    int32_t ret = hal_read_write(OP_FOTA_CHECK_AT_BOOT_GET, 0, NULL, 0, p_check_at_boot, 1);

    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_pwr_policy_set(uint8_t pwr_policy)
{
    int32_t ret = hal_read_write(OP_PWR_POLICY_SET, 0, &pwr_policy, 1, NULL, 0);
    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_pwr_policy_get(uint8_t *p_pwr_policy)
{
    if (p_pwr_policy == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }
    int32_t ret = hal_read_write(OP_PWR_POLICY_GET, 0, NULL, 0, p_pwr_policy, 1);

    return (ret >= 0) ? LTE_IFC_ACK : ret;
}


int32_t lte_ifc_network_info_get(uint32_t* p_u32_time_utc_last_rx_s,
                                 uint32_t* p_u32_time_utc_now_s,
                                 int8_t*   p_i8_ofst_qhrs,
                                 int16_t*  p_i16_rsrp_dbm,
                                 int16_t*  p_i16_rsrq_db,
                                 uint32_t* p_u32_cell_id,
                                 uint16_t* p_u16_cell_tac)
{
    static const int32_t net_info_len = 19;
    uint8_t rsp_buf[net_info_len];
    memset(rsp_buf, 0, net_info_len);
    int32_t ret = hal_read_write(OP_NETWORK_INFO_GET, 0, NULL, 0, rsp_buf, net_info_len);

    if (p_u32_time_utc_last_rx_s != NULL)
    {
        *p_u32_time_utc_last_rx_s =  ((uint32_t)rsp_buf[0]) << 24;
        *p_u32_time_utc_last_rx_s += ((uint32_t)rsp_buf[1]) << 16;
        *p_u32_time_utc_last_rx_s += ((uint32_t)rsp_buf[2]) << 8;
        *p_u32_time_utc_last_rx_s += ((uint32_t)rsp_buf[3]);
    }
    if (p_u32_time_utc_now_s != NULL)
    {
        *p_u32_time_utc_now_s =  ((uint32_t)rsp_buf[4]) << 24;
        *p_u32_time_utc_now_s += ((uint32_t)rsp_buf[5]) << 16;
        *p_u32_time_utc_now_s += ((uint32_t)rsp_buf[6]) << 8;
        *p_u32_time_utc_now_s += ((uint32_t)rsp_buf[7]);
    }
    if (p_i8_ofst_qhrs != NULL)
    {
        *p_i8_ofst_qhrs =  rsp_buf[8];
    }
    if (p_i16_rsrp_dbm != NULL)
    {
        *p_i16_rsrp_dbm =  ((uint16_t)rsp_buf[9]) << 8;
        *p_i16_rsrp_dbm += ((uint16_t)rsp_buf[10]);
    }
    if (p_i16_rsrq_db != NULL)
    {
        *p_i16_rsrq_db =  ((uint16_t)rsp_buf[11]) << 8;
        *p_i16_rsrq_db += ((uint16_t)rsp_buf[12]);
    }
    if (p_u32_cell_id != NULL)
    {
        *p_u32_cell_id =  ((uint32_t)rsp_buf[13]) << 24;
        *p_u32_cell_id += ((uint32_t)rsp_buf[14]) << 16;
        *p_u32_cell_id += ((uint32_t)rsp_buf[15]) << 8;
        *p_u32_cell_id += ((uint32_t)rsp_buf[16]);
    }
    if (p_u16_cell_tac != NULL)
    {
        *p_u16_cell_tac =  ((uint16_t)rsp_buf[17]) << 8;
        *p_u16_cell_tac += ((uint16_t)rsp_buf[18]);
    }

    return (ret >= 0) ? LTE_IFC_ACK : ret;
}

int32_t lte_ifc_network_info_update(uint32_t* p_request_id)
{
    if (p_request_id == NULL)
    {
        return LTE_IFC_ACK_CODE_INCORRECT_PARAMETER;
    }

    uint8_t temp[4] = {0};

    int32_t ret = hal_read_write(OP_NETWORK_INFO_UPDATE, 0, NULL, 0, temp, 4);
    if (ret > 0)
    {
        (*p_request_id) = 0;
        (*p_request_id) += ((uint32_t)(temp[3])) <<  0;
        (*p_request_id) += ((uint32_t)(temp[2])) <<  8;
        (*p_request_id) += ((uint32_t)(temp[1])) << 16;
        (*p_request_id) += ((uint32_t)(temp[0])) << 24;
        return LTE_IFC_ACK;
    }
    else
    {
        return ret;
    }
}

static int32_t lte_ifc_is_time_expired(const struct lte_ifc_time* start_time,
                             const struct lte_ifc_time* timeout, bool* p_is_expired)
{
    *p_is_expired = false;

    struct lte_ifc_time curr_time;
    if(lte_ifc_hal_gettime(&curr_time) != 0)
    {
        return LTE_IFC_ACK_CODE_GETTIME;
    }
//    printf("curr_time = %d.%d\n", curr_time.tv_sec, curr_time.tv_nsec);
    
    struct lte_ifc_time elapsed_time;
    
    elapsed_time.tv_sec = curr_time.tv_sec - start_time->tv_sec;

    if (curr_time.tv_nsec >= start_time->tv_nsec)
    {
        // Easy case
        elapsed_time.tv_nsec = curr_time.tv_nsec - start_time->tv_nsec;
    }
    else
    {
        // Hard case
        uint32_t tmp = ((uint32_t)1e9) - start_time->tv_nsec;
        elapsed_time.tv_nsec = tmp + curr_time.tv_nsec;

        elapsed_time.tv_sec -= 1;
    }    
    
    // OK, now we know elapsed_time and timeout. Check for expiration
    if (elapsed_time.tv_sec > timeout->tv_sec)
    {
        *p_is_expired = true;
    }
    else if (elapsed_time.tv_sec < timeout->tv_sec)
    {
        *p_is_expired = false;
    }
    else
    {
        *p_is_expired = (elapsed_time.tv_nsec >= timeout->tv_nsec );
    }

//    printf("%s expired: elapsed = %d.%d, timeout = %d.%d\n",
//           (*p_is_expired) ? " " : "NOT",
//                   elapsed_time.tv_sec, elapsed_time.tv_nsec,
//                   timeout->tv_sec, timeout->tv_nsec);

    return 0;
}

/**
 * @brief
 *  send_packet
 *
 * @param[in] op
 *   opcode of the command being sent to the module
 *
 * @param[in] ver
 *   version of the command being sent to the module
 *
 * @param[in] message_num
 *   message_num
 *
 * @param[in] buf
 *   byte array containing the data payload to be sent to the module
 *
 * @param[in] len
 *   size of the output buffer in bytes
 *
 * @return
 *   none
 */
static int32_t send_packet(opcode_t op, uint8_t ver, uint8_t message_num, uint8_t *buf, uint16_t len)
{
    #define SP_HEADER_SIZE (HOST_IFC_CMD_HEADER_LEN)
    uint8_t header_buf[SP_HEADER_SIZE];
    uint8_t checksum_buff[2];
    uint16_t computed_checksum;
    uint16_t header_idx = 0;

    // Send wakeup request, and wait up to 100ms for wakeup status
    if(lte_ifc_hal_set_wake_request(true) != 0)
    {
        return LTE_IFC_ACK_CODE_WAKEUP_SET;
    }

    const struct lte_ifc_time timeout = {.tv_sec = 1, .tv_nsec = 0};
    struct lte_ifc_time start_time;
    if (lte_ifc_hal_gettime(&start_time) != 0)
    {
        return LTE_IFC_ACK_CODE_GETTIME;
    }

    bool waiting_on_wake_status = true;
    while (waiting_on_wake_status)
    {
        bool b_expired = false;
        int32_t expired_ret = lte_ifc_is_time_expired(&start_time, &timeout, &b_expired);
        if (expired_ret != 0)
        {
            return expired_ret;
        }
        if(b_expired)
        {
            return LTE_IFC_ACK_CODE_WAKEUP_TIMEOUT;
        }

        bool wake_status = false;
        if (lte_ifc_hal_get_wake_status(&wake_status) != 0)
        {
            return LTE_IFC_ACK_CODE_WAKEUP_GET;
        }

        if (wake_status)
        {
            break;
        }
    }

    header_buf[header_idx++] = HOST_IFC_BYTE_SOF;
    header_buf[header_idx++] = op;
    header_buf[header_idx++] = ver;
    header_buf[header_idx++] = message_num;
    header_buf[header_idx++] = (uint8_t)(0xFF & (len >> 8));
    header_buf[header_idx++] = (uint8_t)(0xFF & (len >> 0));

    computed_checksum = compute_checksum(header_buf, HOST_IFC_CMD_HEADER_LEN, buf, len);

    if(lte_ifc_hal_write(header_buf, SP_HEADER_SIZE) != 0)
    {
        return LTE_IFC_ACK_CODE_WRITE;
    }

    if (buf != NULL)
    {
        if (lte_ifc_hal_write(buf, len) != 0)
        {
            return LTE_IFC_ACK_CODE_WRITE;
        }
    }

    checksum_buff[0] = (computed_checksum >> 8);
    checksum_buff[1] = (computed_checksum >> 0);

    if (lte_ifc_hal_write(checksum_buff, 2) != 0)
    {
        return LTE_IFC_ACK_CODE_WRITE;
    }

    return 0;
}

/**
 * @brief
 *   recv_packet
 *
 * @param[in] op
 *   opcode of the command that we're trying to receive
 *
 * @param[in] message_num
 *   message number of the command that we're trying to receive
 *
 * @param[in] buf
 *   byte array for storing data returned from the module
 *
 * @param[in] len
 *   size of the output buffer in bytes
 *
 * @return
 *   positive number of bytes returned,
 *   negative if an error
 *   Error Codes:
 */
static int32_t recv_packet(opcode_t op, uint8_t message_num, uint8_t *buf, uint16_t len)
{
    uint8_t  header_buf[HOST_IFC_RESP_HEADER_LEN];
    uint16_t header_idx;

    uint8_t curr_byte = 0;
    uint8_t checksum_buff[2];
    uint16_t computed_checksum;
    int32_t ret_value = 0;
    int32_t ret;

    int32_t expired_ret = 0;
    bool b_expired = false;

    memset(header_buf, 0, sizeof(header_buf));
    
    const struct lte_ifc_time timeout = {.tv_sec = 0, .tv_nsec = (uint32_t)3e8};
    struct lte_ifc_time start_time;
    if(lte_ifc_hal_gettime(&start_time) != 0)
    {
        return LTE_IFC_ACK_CODE_GETTIME;
    }

    do
    {
        /* Timeout of infinite Rx loop if responses never show up*/
        uint8_t tmp_byte = 0;
        ret = lte_ifc_hal_read_byte(&tmp_byte);
//        printf("\t\trx: read_byte returned %d\n", ret);

        if (ret == 0)
        {
            curr_byte = tmp_byte;
        }
        else
        {
            expired_ret = lte_ifc_is_time_expired(&start_time, &timeout, &b_expired);

//            printf("\t\trx: time expired returned %d, b_expired = %d\n", expired_ret, b_expired);

            if (expired_ret != 0)
            {
                return expired_ret;
            }
            if(b_expired)
            {
                len = 0;
                return LTE_IFC_ACK_CODE_START_OF_FRAME;
            }
        }
    } while(curr_byte != HOST_IFC_BYTE_SOF);

    // printf("\t\trx: got SOF\n");

    header_idx = 0;
    header_buf[header_idx++] = HOST_IFC_BYTE_SOF;

    do
    {
        /* Timeout of infinite Rx loop if responses never show up*/
        ret = lte_ifc_hal_read_byte(&curr_byte);
        if (ret == 0)
        {
            header_buf[header_idx++] = curr_byte;
        }
        else
        {
            expired_ret = lte_ifc_is_time_expired(&start_time, &timeout, &b_expired);
            if (expired_ret != 0)
            {
                return expired_ret;
            }
            if(b_expired)
            {
                len = 0;
                return LTE_IFC_ACK_CODE_HOST_INTERFACE_TIMEOUT;
            }
        }
    } while(header_idx < HOST_IFC_RESP_HEADER_LEN);

    uint16_t len_from_header = (uint16_t)header_buf[5] + ((uint16_t)header_buf[4] << 8);

    // printf("\t\t rx: got header len = %d\n", len_from_header);
    // printf("\t\t rx: header_buf[1] = 0X%02X\n", header_buf[1]);
    // printf("\t\t rx: header_buf[2] = 0X%02X\n", header_buf[2]);
    // printf("\t\t rx: header_buf[3] = 0X%02X\n", header_buf[3]);

    if (header_buf[1] != op)
    {
        // Command Byte should match what was sent
        ret_value = LTE_IFC_ACK_CODE_COMMAND_MISMATCH;
    }
    if (header_buf[2] != message_num)
    {
        // Message Number should match
        ret_value = LTE_IFC_ACK_CODE_MESSAGE_NUMBER_MISMATCH;
    }
    if (header_buf[3] != LTE_IFC_ACK)
    {
        // NACK Received
        // Map NACK code to error code and pass back
        ret_value = 0 - (int32_t)header_buf[3];
    }
    if (len_from_header > len)
    {
        // response is larger than the caller expects.
        // Pull the bytes out of the Rx fifo
        int32_t ret;
        do
        {
            uint8_t temp_byte;
            ret = lte_ifc_hal_read_byte(&temp_byte);
        }
        while (ret == 0);
        return LTE_IFC_ACK_CODE_BUFFER_TOO_SMALL;
    }
    else if (len_from_header < len)
    {
        // response is shorter than caller expects.
        len = len_from_header;
    }
    
    if (ret_value == 0)
    {
        // If we got here, then we:
        // 1) Received the FRAME_START in the response
        // 2) The message number matched
        // 3) The ACK byte was ACK (not NACK)
        // 4) The received payload length is less than or equal to the size of the buffer
        //      allocated for the payload
        
        uint16_t bytes_written_out = 0;
        uint8_t tmp_byte_out = 0;

        while(bytes_written_out < (len+2))
        {
            /* Timeout of infinite Rx loop if responses never show up*/
            ret = lte_ifc_hal_read_byte(&tmp_byte_out);
            if (ret == 0)
            {
                if (bytes_written_out<len)
                {
                    buf[bytes_written_out++] = tmp_byte_out;
                }   
                else
                {
                    checksum_buff[bytes_written_out-len] = tmp_byte_out;
                    bytes_written_out++;
                }
            }
            else
            {
                expired_ret = lte_ifc_is_time_expired(&start_time, &timeout, &b_expired);
                if (expired_ret != 0)
                {
                    return expired_ret;
                }
                if(b_expired)
                {
                    len = 0;
                    return LTE_IFC_ACK_CODE_HOST_INTERFACE_TIMEOUT;
                }
            }
        }
    }

    if (ret_value == 0)
    {
        computed_checksum = compute_checksum(header_buf, HOST_IFC_RESP_HEADER_LEN, buf, len);
        uint16_t rx_checksum = ((uint16_t)checksum_buff[0] << 8) + checksum_buff[1];
        if (rx_checksum != computed_checksum)
        {
            return LTE_IFC_ACK_CODE_CHECKSUM_MISMATCH;
        }
    }

    if (ret_value == 0)
    {
        // Success! Return the number of bytes in the payload (0 or positive number)
        return len;
    }
    else
    {
        // Failure! Return an error, such as NACK response from the firmware
        return ret_value;
    }
}

/**
 * @brief
 *   compute_checksum
 *
 * @param[in] hdr
 *   header array to compute checksum on
 *
 * @param[in] hdr_len
 *   size of the header array in bytes
 *
 * @param[in] payload
 *   payload array to compute checksum on
 *
 * @param[in] payload_len
 *   size of the payload array in bytes
 *
 * @return
 *   The 8-bit checksum
 */
static uint16_t compute_checksum(uint8_t *hdr, uint16_t hdr_len, uint8_t *payload, uint16_t payload_len)
{
    uint16_t crc = 0x0;
    uint16_t i;

    for (i = 0; i < hdr_len; i++)
    {
        crc  = (crc >> 8) | (crc << 8);
        crc ^= hdr[i];
        crc ^= (crc & 0xff) >> 4;
        crc ^= crc << 12;
        crc ^= (crc & 0xff) << 5;
    }

    for (i = 0; i < payload_len; i++)
    {
        crc  = (crc >> 8) | (crc << 8);
        crc ^= payload[i];
        crc ^= (crc & 0xff) >> 4;
        crc ^= crc << 12;
        crc ^= (crc & 0xff) << 5;
    }

    return crc;
}
